*** Settings ***
Library     Selenium2Library
Resource          resource.robot
Suite Setup       Login
Test Template     Unsuccessful Search

*** Test Cases ***      Search
No Result Search        qpojdioqwndoqin

*** Keywords ***
Login
    Open Browser To URL
    Login User

Unsuccessful Search
    [Arguments]     ${TESTCASE}
    Search      ${TESTCASE}
    Wait Until Element Is Visible   //*[@id="page-skin"]/div[2]/div/div[3]/h1/span[1]
    Element Should Contain     //*[@id="page-skin"]/div[2]/div/div[3]/h1/span[1]      0 rezultate pentru:
