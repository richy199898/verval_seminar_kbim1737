*** Settings ***
Resource          resource.robot
Library          Library.py


*** Test Cases ***
Check Cart
    Open Browser To URL
    Login User
    Go To Laptops
    Add Laptop To Cart


Remove From Cart
    Open Browser To URL
    Login User
    Go To Laptops
    Add Product to Cart
    Remove Product from Cart

Only Apple Laptops
    Open Browser To URL
    Login User
    Go To Laptops
    Apple Laptops


Add To Favorits
    Open Browser To URL
    Login User
    Go To Laptops
    Add Laptop To Favorits

Remove Laptop From Favorits
    Open Browser To URL
    Login User
    Remove From Favorits


Make New Shopping List
    Open Browser To URL
    Login User
    Go To Supermarket
    Go To Shopping List
    Make New List

Delete Shopping List
    Open Browser To URL
    Login User
    Go To Supermarket
    Go To Shopping List
    Delete List


Add New Location
    Open Browser To URL
    Login User
    Go To Profile
    Add Delivery Location

Delete Location
    Open Browser To URL
    Login User
    Go To Profile
    Delete Delivery Location

Increment product
    Sign in user
    Navigate to supermarket ulei
    Increment
