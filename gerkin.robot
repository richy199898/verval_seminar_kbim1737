*** Settings ***
Documentation   Gherkin test
Library         Selenium2Library
Resource        resource.robot
Test Setup      Open Browser To Url

*** Test Cases ***
Add product to cart
    Given I log in
    When I go to ps4 consoles
    Then I expect to see ps4 consoles


*** Keywords ***
I log in
    Login User

I go to ps4 consoles
    Go To PS4 Consoles

I expect to see ps4 consoles
    Check PS4 Console