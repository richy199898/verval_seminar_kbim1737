*** Settings ***
Library     Selenium2Library
Resource          resource.robot
Suite Setup       Login
Test Template     Bad Input Data

*** Test Cases ***      Change Phone Number
Input                   qwerty

*** Keywords ***
Login
    Open Browser To URL
    Login User
    Go To Profile
Bad Input Data
    [Arguments]     ${TESTCASE}
    Change Phone Number      ${TESTCASE}
    Wait Until Element Is Visible   //*[@id="detailsForm"]/fieldset/div[4]/div/span[2]
    Element Should Contain     //*[@id="detailsForm"]/fieldset/div[4]/div/span[2]      Te rugăm să introduci un număr de telefon valid

