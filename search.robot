*** Settings ***
Library     Selenium2Library
Resource          resource.robot
Suite Setup       Login
Test Template     Search Successful

*** Test Cases ***      Search
Result Search           galaxy s20

*** Keywords ***
Login
    Open Browser To URL
    Login User
Search Successful
    [Arguments]     ${TESTCASE}
    Search      ${TESTCASE}
    Wait Until Element Is Visible   //*[@id="page-skin"]/div[2]/div/div[3]/div[2]/div[2]/div[1]/h1/span[1]
    Should Not Contain     //*[@id="page-skin"]/div[2]/div/div[3]/div[2]/div[2]/div[1]/h1/span[1]      0 rezultate

