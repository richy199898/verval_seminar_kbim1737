*** Settings ***
Documentation       A resource file with keywords
Library     Selenium2Library

*** Variables ***
${USERNAME}  valamivalaki@gmail.com
${PASSWORD}  Qwerty123
${DELAY}     1 seconds
${PRICE}
${FINALPRICE}
${LISTNAME}     myFirstList
${FULLNAME}     Valaki Valami
${PHONE}        0712345678
${ADRESS}       str. Horea nr.1



*** Keywords ***
Open Browser To URL
    Open Browser    https://www.emag.ro     browser=chrome
    Delete All Cookies
    Maximize Browser Window
    Set Selenium Speed      ${DELAY}


Login User
    Click Element      //*[@id="my_account"]/i
    Input Text      //*[@id="email"]    ${USERNAME}
    Click Element   //button[contains(text(), 'Continua')]
    Wait Until Element Is Visible   //*[@id="password"]
    Input Text      //*[@id="password"]     ${PASSWORD}
    Click Element   //button[contains(text(), 'Continua')]

Go To Laptops
    Wait Until Element Is Visible   //span[contains(text(), 'Laptop, Tablete')]
    Click Element   //span[contains(text(), 'Laptop, Tablete')]
    Wait Until Element Is Visible   //*[@id="emg-body-overlay"]/div[9]/div/div[3]/aside/ul/li[1]/a
    Click Element   //*[@id="emg-body-overlay"]/div[9]/div/div[3]/aside/ul/li[1]/a
    Wait Until Element Is Visible   //*[@id="emg-body-overlay"]/div[7]/div/div[3]/aside/ul[1]/li[1]/a
    Click Element   //*[@id="emg-body-overlay"]/div[7]/div/div[3]/aside/ul[1]/li[1]/a

Add Laptop To Cart
    Wait Until Element is Visible   //*[@id="card_grid"]/div[1]/div/div/div[3]/div[2]/p[2]
    ${PRICE}=   Get Text    //*[@id="card_grid"]/div[1]/div/div/div[3]/div[2]/p[2]
    Click Element   //*[@id="card_grid"]/div[1]/div/div/div[3]/div[3]/form/button
    Wait Until Element Is Visible   //html/body/div[14]/div/div/div[1]/button
    Click Element   //html/body/div[14]/div/div/div[1]/button
    Go To   https://www.emag.ro
    Wait Until Element Is Visible   //*[@id="my_cart"]
    Click Element   //*[@id="my_cart"]
    Wait Until Element Is Visible   //*[@id="cartProductsPage"]/div[1]/div[1]/div/div[3]/p
    ${FINALPRICE}=  Get Text    //*[@id="cartProductsPage"]/div[1]/div[1]/div/div[3]/p
    Should Be Equal As Strings      ${PRICE}    ${FINALPRICE}

Add Product to Cart
    Wait Until Element Is Visible   //*[@id="card_grid"]/div[1]/div/div/div[3]/div[2]/p[2]
    Click Element   //*[@id="card_grid"]/div[1]/div/div/div[3]/div[3]/form/button
    Wait Until Element Is Visible   //html/body/div[14]/div/div/div[1]/button
    Click Element   //html/body/div[14]/div/div/div[1]/button

Remove Product from Cart
    Go To   https://www.emag.ro
    Wait Until Element is Visible   //*[@id="my_cart"]
    Click Element   //*[@id="my_cart"]
    Wait Until Element Is Visible   //*[@id="vendorsContainer"]/div/div[1]/div[1]/div[2]/div[1]/div[3]/a[1]
    Click Element   //*[@id="vendorsContainer"]/div/div[1]/div[1]/div[2]/div[1]/div[3]/a[1]
    Wait Until Element Is Visible   //*[@id="empty-cart"]/div[1]
    Element Should Contain     //*[@id="empty-cart"]/div[1]    Cosul tau este gol

Apple Laptops
    Wait Until Element Is Visible   //*[@id="js-filter-8354-collapse"]/div[2]/a[5]
    Click Element   //*[@id="js-filter-8354-collapse"]/div[2]/a[5]
    Wait Until Element Is Visible   //*[@id="card_grid"]/div[1]/div[2]/div/div[2]/h2/a
    Element Should Contain      //*[@id="card_grid"]/div[1]/div[2]/div/div[2]/h2/a      Laptop Apple
    Wait Until Element is Visible   //*[@id="card_grid"]/div[8]/div[2]/div/div[2]/h2/a
    Element Should Contain      //*[@id="card_grid"]/div[8]/div[2]/div/div[2]/h2/a      Laptop Apple


Add Laptop To Favorits
    Wait Until Element Is Visible   //*[@id="card_grid"]/div[1]/div[2]/div/div[1]/div[2]/button[2]/i
    Click Element   //*[@id="card_grid"]/div[1]/div[2]/div/div[1]/div[2]/button[2]/i
    Go To       https://www.emag.ro
    Wait Until Element Is Visible   //*[@id="my_wishlist"]
    Click Element   //*[@id="my_wishlist"]
    Wait Until Element Is Visible   //*[@id="main-container"]/section/div[1]/div[2]/div/div[4]/div[1]/div
    Element Should Contain      //*[@id="main-container"]/section/div[1]/div[2]/div/div[4]/div[1]/div   1 produs

Remove From Favorits
    Wait Until Element Is Visible   //*[@id="my_wishlist"]
    Click Element   //*[@id="my_wishlist"]
    Wait Until Element Is Visible   //*[@id="list-of-favorites"]/div/div[2]/div[2]/div[2]/div[5]/div[2]/button/span
    Click Element   //*[@id="list-of-favorites"]/div/div[2]/div[2]/div[2]/div[5]/div[2]/button/span
    Wait Until Element Is Visible   //*[@id="main-container"]/section/div[1]/div[2]/div/div[4]/div[1]/div
    Element Should Contain      //*[@id="main-container"]/section/div[1]/div[2]/div/div[4]/div[1]/div   0 produse

Search
    [Arguments]     ${TESTCASE}
    Wait Until Element Is Visible   //*[@id="searchboxTrigger"]
    Input text     //*[@id="searchboxTrigger"]     ${TESTCASE}
    Click Element   //*[@id="masthead"]/div/div/div[2]/div/form/div[1]/div[2]/button[2]

Go To PS4 Consoles
    Wait Until Element Is Visible   //html/body/div[4]/div[1]/div/div[1]/ul/li[5]/a
    Click Element   //html/body/div[4]/div[1]/div/div[1]/ul/li[5]/a
    Wait Until Element Is Visible   //*[@id="emg-body-overlay"]/div[9]/div/div[3]/aside/ul/li[2]/a
    Click Element   //*[@id="emg-body-overlay"]/div[9]/div/div[3]/aside/ul/li[2]/a
    Wait Until Element Is Visible   //*[@id="emg-body-overlay"]/div[7]/div/div[3]/aside/ul[2]/li[2]/a
    Click Element   //*[@id="emg-body-overlay"]/div[7]/div/div[3]/aside/ul[2]/li[2]/a


Check PS4 Console
    Wait Until Element Is Visible   //*[@id="card_grid"]/div[1]/div/div/div[2]/h2/a
    Element Should Contain      //*[@id="card_grid"]/div[1]/div/div/div[2]/h2/a     Consola SONY PlayStation 4

Go To Supermarket
    Wait Until Element Is Visible   //html/body/div[4]/div[1]/div/div[1]/ul/li[13]/a
    Click Element       //html/body/div[4]/div[1]/div/div[1]/ul/li[13]/a

Go To Shopping List
    Wait Until Element Is Visible   //*[@id="emg-user-fav"]
    Click Element   //*[@id="emg-user-fav"]

Make New List
    Wait Until Element Is Visible   //*[@id="main-container"]/section/div[1]/div[2]/div/div[1]/div[2]/button
    Click Element       //*[@id="main-container"]/section/div[1]/div[2]/div/div[1]/div[2]/button
    Wait Until Element Is Visible   //*[@id="list-name"]
    Input Text  //*[@id="list-name"]    ${LISTNAME}
    Click Element   //button[contains(text(), 'Salveaza')]
    Wait Until Element Is Visible   //*[@id="list-items-collapse"]/div[2]/a/p[1]
    Element Should Contain      //*[@id="list-items-collapse"]/div[2]/a/p[1]    ${LISTNAME}


Delete List
    Wait Until Element Is Visible   //*[@id="list-items-collapse"]/div[2]/a
    Click Element       //*[@id="list-items-collapse"]/div[2]/a
    Click Element   //*[@id="main-container"]/section/div[1]/div[2]/div/div[4]/div[1]/div[2]/button[1]
    Wait Until Element Is Visible   //html/body/div[17]/div/div/form/div[4]/button[2]
    Click Element   //html/body/div[17]/div/div/form/div[4]/button[2]
    Wait Until Element Is Visible   //html/body/div[18]/div/div/div[2]/div[2]/button[2]
    Click Element   //html/body/div[18]/div/div/div[2]/div[2]/button[2]
    Element Should Not Be Visible   //*[@id="list-items-collapse"]/div[2]/a

Go To Profile
    Wait Until Element Is Visible   //*[@id="my_account"]
    Click Element   //*[@id="my_account"]

Add Delivery Location
    Wait Until Element Is Visible       //*[@id="main-container"]/section/div/div[2]/section/div/div[5]/div/div[2]/a
    Click Element       //*[@id="main-container"]/section/div/div[2]/section/div/div[5]/div/div[2]/a
    Wait Until Element Is Visible   //*[@id="user-name"]
    Input Text  //*[@id="user-name"]    ${FULLNAME}
    Input Text  //*[@id="user-phone"]   ${PHONE}
    Wait Until Element Is Visible      //html/body/div[9]/div/div/div[2]/form/div/div[2]/div[1]/div/div
    Click Element       //html/body/div[9]/div/div/div[2]/form/div/div[2]/div[1]/div/div
    Wait Until Element Is Visible       //html/body/div[10]/div/div[2]/a[1]
    Click Element       //html/body/div[10]/div/div[2]/a[1]
    Wait Until Element Is Enabled       //html/body/div[9]/div/div/div[2]/form/div/div[2]/div[2]/div/div
    Click Element       //html/body/div[9]/div/div/div[2]/form/div/div[2]/div[2]/div/div
    Wait Until Element Is Visible      //html/body/div[11]/div/div[2]/a[1]
    Click Element       //html/body/div[11]/div/div[2]/a[1]
    Wait Until Element Is Visible       //*[@id="user-street"]
    Input Text      //*[@id="user-street"]  ${ADRESS}
    Click Element   //button[contains(text(), 'Salveaza')]
    Wait Until Element Is Visible   //*[@id="main-container"]/section/div/div[2]/section/div/div[5]/div/div[1]/p[2]
    Element Should Contain      //*[@id="main-container"]/section/div/div[2]/section/div/div[5]/div/div[1]/p[2]     1 adresa salvata


Delete Delivery Location
    Wait Until Element Is Visible   //*[@id="main-container"]/section/div/div[2]/section/div/div[5]/div/div[2]/a
    Click Element   //*[@id="main-container"]/section/div/div[2]/section/div/div[5]/div/div[2]/a
    Wait Until Element Is Visible   //span[contains(text(), 'sterge')]
    Click Element   //span[contains(text(), 'sterge')]
    Wait Until Element Is Visible   //*[@id="yes"]
    Click Element   //*[@id="yes"]
    Wait Until Element Is Visible   //*[@id="addressNotAdded"]
    Element Should Contain      //*[@id="addressNotAdded"]      Nu ai adaugat nici o adresa

Change Phone Number
    [Arguments]     ${PHONENR}
    Wait Until Element Is Visible   //*[@id="main-container"]/section/div/div[2]/section/div/div[1]/div/div[2]/a
    Click Element   //*[@id="main-container"]/section/div/div[2]/section/div/div[1]/div/div[2]/a
    Wait Until Element Is Visible   //*[@id="mobilphone"]
    Input Text      //*[@id="mobilphone"]       ${PHONENR}
    Click Element   //*[@id="detailsForm"]/button