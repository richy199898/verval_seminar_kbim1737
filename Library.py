from Selenium2Library import Selenium2Library
from robot.api.deco import keyword
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as cond
from selenium.webdriver.support.ui import WebDriverWait
import sys


class Library(object):
    def __init__(self):
        self.driver = webdriver.Chrome()

    @keyword('Sign in user')
    def sign_in(self):
        driver = self.driver
        driver.get("https://www.emag.ro")
        driver.maximize_window()
        WebDriverWait(driver, 4).until(
            cond.presence_of_element_located((By.XPATH, "//*[@id=\"my_account\"]")))
        driver.find_element_by_xpath("//*[@id=\"my_account\"]").click()
        WebDriverWait(driver, 4).until(
            cond.presence_of_element_located((By.XPATH, "//*[@id=\"email\"]")))
        driver.find_element_by_xpath("//*[@id=\"email\"]").send_keys('valamivalaki@gmail.com')
        element = driver.find_element_by_xpath("//button[contains(text(), 'Continua')]")
        driver.execute_script("arguments[0].click();", element)
        WebDriverWait(driver, 4).until(
            cond.element_to_be_clickable((By.XPATH, "//*[@id=\"password\"]")))
        driver.find_element_by_xpath("//*[@id=\"password\"]").send_keys('Qwerty123')
        element = driver.find_element_by_xpath("//button[contains(text(), 'Continua')]")
        driver.execute_script("arguments[0].click();", element)

    @keyword('Navigate to supermarket ulei')
    def nav_to_supermarket(self):
        driver = self.driver
        WebDriverWait(driver, 4).until(cond.presence_of_element_located((By.XPATH, "//html/body/div[4]/div[1]/div/div[1]/ul/li[13]/a")))
        driver.find_element_by_xpath("//html/body/div[4]/div[1]/div/div[1]/ul/li[13]/a").click()
        WebDriverWait(driver, 4).until(cond.presence_of_element_located((By.XPATH, "//*[@id=\"emg-body-overlay\"]/div/div/div[3]/div/section[1]/div/figure[1]/a")))
        driver.find_element_by_xpath("//*[@id=\"emg-body-overlay\"]/div/div/div[3]/div/section[1]/div/figure[1]/a").click()

    @keyword('Increment')
    def inc_ulei(self):
        driver = self.driver
        WebDriverWait(driver, 4).until(
            cond.element_to_be_clickable((By.XPATH, "//*[@id=\"card_grid\"]/div[1]/div/div/div[3]/div[1]/div/span[3]/button")))
        element = driver.find_element_by_xpath("//*[@id=\"card_grid\"]/div[1]/div/div/div[3]/div[1]/div/span[3]/button")
        db = driver.find_element_by_xpath("//*[@id=\"card_grid\"]/div[1]/div/div/div[3]/div[1]/div/span[2]").text
        driver.execute_script("arguments[0].click();", element)
        db2 = driver.find_element_by_xpath("//*[@id=\"card_grid\"]/div[1]/div/div/div[3]/div[1]/div/span[2]").text
        assert db != db2


    @keyword('Add ulei to cart')
    def add_ulei(self):
        driver = self.driver
        WebDriverWait(driver, 4).until(
            cond.presence_of_element_located(
                (By.XPATH, "//*[@id=\"card_grid\"]/div[1]/div/div/div[3]/div[3]/form/button")))
        element = driver.find_element_by_xpath("//*[@id=\"card_grid\"]/div[1]/div/div/div[3]/div[3]/form/button")
        driver.execute_script("arguments[0].click()", element)
        element = driver.find_element_by_xpath("//*[@id=\"card_grid\"]/div[1]/div/div/div[2]/h2/a")

    # @keyword('Check cart')
    # def check_cart(self):
    #     driver = self.driver
    #     WebDriverWait(driver, 4).until(
    #         cond.presence_of_element_located(
    #             (By.XPATH, "//*[@id=\"my_cart\"]")))
    #     driver.find_element_by_xpath("//*[@id=\"my_cart\"]").click()
    #     WebDriverWait(driver, 4).until(cond.presence_of_element_located((By.XPATH, "//*[@id=\"vendorsContainer\"]/div/div[1]/div/div[2]/div[1]/div[1]/a")))
    #     element2 = driver.find_element_by_xpath("//*[@id=\"vendorsContainer\"]/div/div[1]/div/div[2]/div[1]/div[1]/a")
    #     assert element.text == element2.text


