*** Settings ***
Library     Selenium2Library
Resource          resource.robot
Suite Setup       Login
Test Template     Correct Input Data

*** Test Cases ***      Change Phone Number
Input                   0712345678

*** Keywords ***
Login
    Open Browser To URL
    Login User
    Go To Profile
Correct Input Data
    [Arguments]     ${TESTCASE}
    Change Phone Number      ${TESTCASE}
    Wait Until Element Is Visible   //*[@id="detailsForm"]/fieldset/div[2]/div/span
    Element Should Contain     //*[@id="detailsForm"]/fieldset/div[2]/div/span      ${TESTCASE}

